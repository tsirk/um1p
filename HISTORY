1. CHANGES

* Made it possible to complete the Watchstones' grand finale.

**  SWILHUN-1 (Wild One) is now located a few hundred units further north,
    next to YRECHU-2 (Reckoner), and shouldn't fall into lava anymore.

**  Renamed one of the two FINSWIL-1 groups (the one next to the lava lake in
    the west) of the Wind Ones to FINSWIL-3 in order to avoid name collision,
    causing only one of the groups to respawn at the time which seems to also
    reset their rating sometimes, affecting the clan's overall rating.

**  Enabled two groups of previously hidden Wild Ones -- SWIRE and SWILHUN
    that were supposed to be enabled in a cut mission named "Bait"
    (T_L4_STONES1). Since the mission is skipped, and the mechminds were never
    enabled, it was impossible to complete "Cripple the Wilds" mission due to
    these droids holding their clan's rating above 3.

* Worked around bugs in game's scripting language implementation logic that
  prevented players from joining clans that had joining conditions for specific
  gliders (the First Ones and Sinigers).
  See `BUGS` for more details.

* Made it possible to complete the Carriers' final mission in Desert Sector
  without utilizing exploits by relocating out of bounds SACREFI-2 of the Black
  Players inside sector's contour field.

* Fixed a mistyped goods ID (TOV_POLYMER_PLATES at PTR_L5_SECRET_19) in
  Desert Sector's main script that instead spawned a droid.

* Fixed a few mistyped goods IDs (TOV_POLYMER_PLATES at PTR_L6_SECRET_{08,29} &
  TOV_MULTIMASS0 at PTR_L6_SECRET_54) in Highlands Sector's main script that
  instead spawned droids.


2. WIP PATCHES

* `loc4_relocate_SWILHUN-3.diff`:
  Relocated SWILHUN-3 of the Wild Ones on the hillside between the raw
  materials base and the main base in Volcano Sector in order to free it from
  the neutralizing effect of rescue area, and scatter Wild Ones out more due to
  SWILHUN and SWIRE now also being present in the area.

* `loc6_half-stock_plant_correct_model.diff`:
  Replaced Highlands Sector half-stock plant model with the correct one.
