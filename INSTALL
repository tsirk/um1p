1. REQUIREMENTS

* `xxd`
* `patch` or `Git` (optional, for applying additional diff-compatible patches)

To revert hex dumps of files back to binary form, you must have `xxd` utility.

On UNIX-like operating systems it's either bundeled with `vim` or `vim-core`
package.

Its source code lives in:

    https://raw.githubusercontent.com/vim/vim/master/src/xxd/xxd.c


2. PATCHING

Patches are unfinised (or purposely separated) changes that haven't been merged
into hex dumps, and are subject to change in the future.

See `HISTORY` for list of patches. If you're not interested of any, skip this
section.

Example with `patch`: `patch < patches/loc4_relocate_SWILHUN-3.diff`

Example with Git: `git apply patches/loc4_relocate_SWILHUN-3.diff`


3. REVERTING

To revert a file back to binary, enter:

    `xxd -r [hex dump] > [output file without the .xxd exension]`

For example: `xxd -r location4.mmo.xxd > location4.mmo`


4. INSTALLATION

This is the tricky part as paths where to place files are mostly not what you
probably expect them to be. Below is list of paths for different asset types,
starting point is root (marked with leading `/`) of the game directory where
`AIM.exe` is located.

* MMM (minimap image): `/`
* MMO (map objects): `/`
* MMP (map terrain): `/`
* Models: `/Data/Models`
* Scripts (source code): `/Script`
* Scripts (binary): `/Script/bin`
* Sounds: `/Data/Sound`
* TM (textures): `/Data/TM`

NB! Since load order of assets is unmanageable, PAK-archives have low priority
due to being buried in `Data`. Once they're loaded their data will overwrite
assets with shared name loaded before them (including patched map and script
files in root). Workaround for this is to move all offending PAK-archives to
somewhere else from `Data`, extract their contents in aformentioned paths,
and then install the patched assets.
While doing so, it's important to do this in the chronological order in which
official patches were released in order to avoid accidentally using older assets.

First, extract base game (whatever that might be, v1.00 or v1.03), then install
next patch and repeat until all official patches have been applied, and their
PAK-archives extracted.

Archives such as `sounds.pak`, `minimaps.pak` and `netminis.pak` can be left
in place and don't have to be extracted as currently no content of theirs needs
replacing.

v0.21 (demo, unimportant for most people)
* models.pak
* tms.pak

v1.00 & v1.03
* maps.pak
* maps2.pak
* netmaps.pak
* netscript.pak
* pack3.pak
* res0.pak
* res1.pak
* res2.pak
* scripts.pak

v1.02/a
* pack3.pak
* scripts.pak

v1.04
* res3.pak
* scripts.pak
